""" Script pour tester l'activation et la configuration de Pylint"""

import time
import math

print(f"now == {time.gmtime(0)}")
print(f"pi = {math.pi}")

def ma_fonction():
    print("fonction executée")
    return 2

ma_fonction()


# now this should fail : 
# because b doesn't exist
print(1 + 2)


