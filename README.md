[![Build status](https://gitlab.inria.fr//test-pylint/badges/master/build.svg)](https://gitlab.inria.fr//test-pylint/commits/master)


This is a sandbox to test how to set up Pylint in CI (Jenkins)

## most useful links

- https://inria-ci.gitlabpages.inria.fr/doc/page/slaves_access_tutorial/
- https://inria-ci.gitlabpages.inria.fr/doc/page/jenkins_tutorial/
- https://inria-ci.gitlabpages.inria.fr/doc/page/using_jenkins_with_gitlab/
- https://inria-ci.gitlabpages.inria.fr/doc/page/faq/
- **https://iww.inria.fr/sed-sophia/gitlab-inria-frci-inria-fr-integration/**

doc : 
- https://iww.inria.fr/sed-sophia/gitlab-inria-frci-inria-fr-integration/
- https://inria-ci.gitlabpages.inria.fr/doc/page/using_jenkins_with_gitlab/#using-jenkins-ci-with-a-gitlab-repository
- https://inria-ci.gitlabpages.inria.fr/doc/page/slaves_access_tutorial/
- https://inria-ci.gitlabpages.inria.fr/doc/page/faq/
- https://ci.inria.fr/dashboard

other : 
- http://sed.bordeaux.inria.fr/org/gitlab-ci-nojenkins.html

pylint : 
- https://www.pylint.org/

flake8 :
- https://flake8.pycqa.org/en/latest/index.html

