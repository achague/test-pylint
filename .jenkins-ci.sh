#!/bin/sh

# requirements sur slave
#(sudo) apt-install python-pip
#(sudo) apt install virtualenv

echo "### ----- BUILDING ENVIRONMENT ----- ###"
virtualenv venv -p python3
. venv/bin/activate  # "source venv/bin/activate" fails
python --version
echo ">> getting requirements"
pip install -r requirements.txt

echo "### ----- LINTER ----- ###"
pylint-fail-under --fail_under 6.0 *.py  # will use .pylintrc in master
# ajust grade is necessary

#echo "### ----- LINTER ----- ###"
# test.py for additional functional tests
#python test.py